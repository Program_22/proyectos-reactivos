package com.app.tarjeta.service;

import com.app.tarjeta.document.Tarjeta;
import com.app.tarjeta.repository.TarjetaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class TarjetaService {
    @Autowired
    private TarjetaRepository tarjetaRepository;

    public Flux<Tarjeta> getAllTarjetas() {
        return tarjetaRepository.findAll();
    }

    public Mono<Tarjeta> getTarjetaById(String id) {
        return tarjetaRepository.findById(id);
    }

    public Mono<Tarjeta> saveTarjeta(Tarjeta tarjeta) {
        return tarjetaRepository.save(tarjeta);
    }

    public Mono<Tarjeta> updateTarjeta(String id, Tarjeta tarjeta) {
        return tarjetaRepository.findById(id)
                .flatMap(existingTarjeta -> {
                    existingTarjeta.setNumeroTarjeta(tarjeta.getNumeroTarjeta());
                    existingTarjeta.setNombreTitular(tarjeta.getNombreTitular());
                    existingTarjeta.setFechaExpiracion(tarjeta.getFechaExpiracion());
                    existingTarjeta.setCvv(tarjeta.getCvv());
                    return tarjetaRepository.save(existingTarjeta);
                });
    }

    public Mono<Void> deleteTarjeta(String id) {
        return tarjetaRepository.deleteById(id);
    }

    public Mono<Boolean> existsByNumeroTarjeta(String numeroTarjeta) {
        return tarjetaRepository.findByNumeroTarjeta(numeroTarjeta)
                .flatMap(tarjeta -> Mono.just(true))
                .switchIfEmpty(Mono.just(false));
    }
}


