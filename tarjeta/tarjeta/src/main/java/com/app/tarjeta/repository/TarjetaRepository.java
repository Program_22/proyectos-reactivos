package com.app.tarjeta.repository;

import com.app.tarjeta.document.Tarjeta;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface TarjetaRepository extends ReactiveMongoRepository<Tarjeta, String> {
    Mono<Tarjeta> findByNumeroTarjeta(String numeroTarjeta);


}
