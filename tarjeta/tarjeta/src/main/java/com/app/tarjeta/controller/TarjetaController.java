package com.app.tarjeta.controller;

import com.app.tarjeta.document.Tarjeta;
import com.app.tarjeta.service.TarjetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/tarjetas")
public class TarjetaController {
    @Autowired
    private TarjetaService tarjetaService;

    @GetMapping
    public Flux<Tarjeta> getAllTarjetas() {
        return tarjetaService.getAllTarjetas();
    }

    @GetMapping("/{id}")
    public Mono<Tarjeta> getTarjetaById(@PathVariable String id) {
        return tarjetaService.getTarjetaById(id);
    }

    @PostMapping
    public Mono<Tarjeta> createTarjeta(@RequestBody Tarjeta tarjeta) {
        return tarjetaService.saveTarjeta(tarjeta);
    }

    @PutMapping("/{id}")
    public Mono<Tarjeta> updateTarjeta(@PathVariable String id, @RequestBody Tarjeta tarjeta) {
        return tarjetaService.updateTarjeta(id, tarjeta);
    }

    @DeleteMapping("/{id}")
    public Mono<Void> deleteTarjeta(@PathVariable String id) {
        return tarjetaService.deleteTarjeta(id);
    }

    @GetMapping("/exists/{numeroTarjeta}")
    public Mono<Boolean> tarjetaExists(@PathVariable String numeroTarjeta) {
        return tarjetaService.existsByNumeroTarjeta(numeroTarjeta);
    }
}

